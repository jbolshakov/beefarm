﻿using BeeFarm.ViewModel;

namespace BeeFarm
{
	/// <summary> Локатор для модели, подключает представление к странице
	/// </summary>
	public class ViewModelLocator
	{
		/// <summary> Передается во вью для связи с представлением
		/// </summary>
		public ApiaryViewModel ViewModel
		{
			get { return new ApiaryViewModel(); }
		}
	}
}
