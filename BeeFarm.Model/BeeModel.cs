﻿using System.Collections.Generic;
using System.Linq;
using BeeFarm.Model.Entity;
using BeeFarm.Model.Storage;

namespace BeeFarm.Model
{
	/// <summary> Базовая объектная модель пасеки
	/// </summary>
    public sealed class BeeModel
    {
		#region Properties
		//Хранилище данных пасеки
		private ApiaryStorage _apiaryStorage;
		//Текущее состояние пасеки
		private readonly ApiaryCache _cache;
		//Хранилище данных пасеки
		public ApiaryStorage ApiaryStorage
		{
			get { return _apiaryStorage; }
		}
		#endregion

		/// <summary> Базовый конструктор для создания модели объектов
		/// </summary>
		/// <param name="hives"></param>
		public BeeModel(Dictionary<int, int> hives)
        {
	        var hivesL = new List<HiveCache>();
	        foreach (var hive in hives)
	        {
		        hivesL.Add(new HiveCache(hive.Key, 0, 0, hive.Value));
	        }
			_cache = new ApiaryCache(0, hivesL);
        }

	    #region Public methods
		/// <summary> Метод загружает текущее состояние пасеки в объектную модель
		/// </summary>
		public void Initialize()
	    {
		    var apiaryStorage = new ApiaryStorage(
			    _cache.AmountCollectedHoney,
			    _cache.HiveList.Select(
				    hive =>
					    new HiveStorage(hive.Number,
						    hive.HoneyCount,
							hive.CollectCount,
						    hive.CollectCount,
						    new QueenBeeEntity(1, hive.Number),
						    this)
			    ).ToList(),
			    this);

			_apiaryStorage = apiaryStorage;
	    }
		#endregion
	}
}
