﻿using System.Collections.Generic;

namespace BeeFarm.Model
{
	/// <summary> Кэш пасеки 
	/// </summary>
    public class ApiaryCache
    {
		#region Properties
		public int AmountCollectedHoney { get; }
		public List<HiveCache> HiveList { get; }
		#endregion

		public ApiaryCache(int amountCollectedHoney, List<HiveCache> hiveList)
	    {
		    AmountCollectedHoney = amountCollectedHoney;
			HiveList = hiveList;
	    }
    }

	/// <summary> Кэш улья
	/// </summary>
    public class HiveCache 
    {
		#region Properties
		public int Number { get; }
		public int HoneyCount { get; }
		public int CollectCount { get; }
		public int SecurityCount { get; }
		#endregion

		public HiveCache(int number, int honeyCount, int collectCount, int securityCount)
	    {
			Number = number;
			HoneyCount = honeyCount;
			CollectCount = collectCount;
			SecurityCount = securityCount;
		}
    }
}
