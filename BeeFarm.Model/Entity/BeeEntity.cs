﻿using System;

namespace BeeFarm.Model.Entity
{
	/// <summary> Объект пчелы
	/// </summary>
    public abstract class BeeEntity : IEquatable<BeeEntity>
    {
	    #region Properties
		//Тип пчелы
	    public abstract BeeTypeEnum Type { get; }
		//Номер пчелы
		public int Number { get; private set; }
		//Номер улья
		public int HiveNumber { get; private set; }
		#endregion

		/// <summary> Конструктор для создания объекта пчелы
		/// </summary>
		/// <param name="number">Номер пчелы</param>
		/// <param name="hiveNumber">номер улья в который помещена пчела</param>
		protected BeeEntity(int number, int hiveNumber)
        {
			Number = number;
			HiveNumber = hiveNumber;
        }

		#region Equals
		public override int GetHashCode()
		{
			return Number ^ (HiveNumber * 31);
		}

		public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(BeeEntity))
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            return Equals((BeeEntity)obj);
        }

        public bool Equals(BeeEntity other)
        {
            if (other == null)
                return false;

            return Number == other.Number && HiveNumber == other.HiveNumber;
        }
		#endregion
	}
}
