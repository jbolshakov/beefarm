﻿namespace BeeFarm.Model.Entity
{
	/// <summary> Объетное представление пчелы генерирущей других пчел
	/// </summary>
	public class QueenBeeEntity : BeeEntity
	{
		#region Properties
		//Тип пчелы
		public override BeeTypeEnum Type
		{
			get { return BeeTypeEnum.Queen; }
		}
		#endregion

		/// <summary> Конструктор для создания объекта пчелы
		/// </summary>
		/// <param name="number">Номер матки</param>
		/// <param name="hiveNumber">Номер улья в который помещена королева</param>
		public QueenBeeEntity(int number, int hiveNumber)
			: base(number, hiveNumber)
		{
		}

		#region Equals
		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != typeof(QueenBeeEntity))
				return false;

			if (ReferenceEquals(this, obj))
				return true;

			return Equals((BeeEntity)obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		#endregion
	}
}
