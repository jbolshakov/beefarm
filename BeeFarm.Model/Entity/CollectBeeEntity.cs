﻿namespace BeeFarm.Model.Entity
{
	/// <summary> Объетное представление пчелы собирающей мед
	/// </summary>
	public class CollectBeeEntity : BeeEntity
	{
		#region Properties
		//Тип пчелы
		public override BeeTypeEnum Type
		{
			get { return BeeTypeEnum.Collect; }
		}
		#endregion

		/// <summary> Конструктор создания собирающей пчелы
		/// </summary>
		/// <param name="number">Номер пчелы</param>
		/// <param name="hiveNumber">Номер улья</param>
		public CollectBeeEntity(int number, int hiveNumber)
			: base(number, hiveNumber)
		{
		}

		#region Equals
		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != typeof(CollectBeeEntity))
				return false;

			if (ReferenceEquals(this, obj))
				return true;

			return Equals((BeeEntity)obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		#endregion
	}
}
