﻿using System.Runtime.Serialization;

namespace BeeFarm.Model
{
	/// <summary> Перечислитель типов пчел
	/// </summary>
	[DataContract(Name = "BeeTypeEnum", Namespace = "BeeFarm.Model")]
	public enum BeeTypeEnum : byte
	{
		/// <summary> Пчела генерирующая других пчел
		/// </summary>
		[EnumMember]
		Queen = 1,
		/// <summary> Пчела приносящая мед в улей
		/// </summary>
		[EnumMember]
		Collect = 2,
		/// <summary> Пчела охранник, не пускает пчел в улей
		/// </summary>
		[EnumMember]
		Security = 3
	}
}
