﻿using System.Collections.Generic;
using BeeFarm.Model.Entity;

namespace BeeFarm.Model.Storage
{
	/// <summary> Хранилище ульев
	/// </summary>
    public class HiveStorage
    {
		#region Properties
		//номер улья
		public int Number { get; private set; }
		//текущее количество собранного меда пчелами
		public int HoneyCount { get; set; }
		//Существующие пчелы охранники
		public volatile List<SecurityBeeEntity> SecurityBees;
		//Существующие собирающие пчелы в улье
		public volatile List<CollectBeeEntity> CollectBees;
		//Королева улья
	    public QueenBeeEntity QueenBeeEntity { get; private set; }
		//Базовая объектная модель пасеки
	    public BeeModel Model { get; set; }
		//Флаг удаленного улья
		public volatile bool IsDeleted;
		//Общее количество пчел в улье
		public int TotalBee { get { return CollectBees.Count + 1; } }
		#endregion

		public HiveStorage(
            int number,
            int honeyCount,
			int securityBeeCount,
			int collectBeeCount,
            QueenBeeEntity queenBeeEntity,
            BeeModel model)
        {
			HoneyCount = honeyCount;
			QueenBeeEntity = queenBeeEntity;
			Model = model;
			Number = number;

			var securityBees = new List<SecurityBeeEntity>();
			for (int i = 0; i < securityBeeCount; i++)
			{
				securityBees.Add(new SecurityBeeEntity(++i + 1, number));
			}
	        SecurityBees = securityBees;

			var collectBee = new List<CollectBeeEntity>();
			for (int i = 0; i < collectBeeCount; i++)
			{
				collectBee.Add(new CollectBeeEntity(++i + 1, number));
			}

			CollectBees = collectBee;
		}

	    #region Public methods
		//последний сгенерированный номер пчелы в улье
		public int GetNumberForNewBee()
	    {
		    return TotalBee;
	    }

		//Генерация меда в улье
	    public void AccumulateHoney()
	    {
		    HoneyCount++;
	    }

		//Добавление пчелы в улье
		public void CreatSecurityBee(SecurityBeeEntity beeEntity)
		{
			SecurityBees.Add(beeEntity);
		}

		//Добавление пчелы в улье
		public void CreateCollectBee(CollectBeeEntity beeEntity)
	    {
		    CollectBees.Add(beeEntity);
	    }

		//Сбор меда с улья
		public int CollectHoney()
		{
			return HoneyCount;
		}
		#endregion
	}
}
