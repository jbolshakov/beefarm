﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeeFarm.Model.Entity;

namespace BeeFarm.Model.Storage
{
	/// <summary> Объектное представление пасеки
	/// </summary>
    public class ApiaryStorage : IDisposable
    {
		#region Properties
		private object _locker = new Object();
		//Общее количество меда собранного со всех ульев
		public volatile int AmountCollectedHoney;
		//Список существующих ульев
	    private volatile List<HiveStorage> _hiveList;
		//Список существующих ульев
		public List<HiveStorage> HiveList
		{
		    get
		    {
			    lock (_locker)
			    {
				    return _hiveList;
			    }
		    }
	    }
		//Базовая объектная модель пасеки
		public BeeModel Model { get; set; }
		//количество пасек в улье
		public int HiveCount { get { return HiveList.Count; } }
		//количество маток генерирующих собирающих мед пчел в пасеке
		public int QueenCount { get { return 1; } }
		//Общее количество пчел в пасеке
		public int BeeCount { get { return HiveList.Sum(row => row.TotalBee); } }
		#endregion

		public ApiaryStorage(
            int amountCollectedHoney,
            List<HiveStorage> hiveList,
            BeeModel model)
	    {
		    AmountCollectedHoney = amountCollectedHoney;
			_hiveList = hiveList;
			Model = model;
	    }

		#region Public methods

		/// <summary> Метод выдает первое улей из существующих
		/// </summary>
		/// <returns></returns>
		public async Task<HiveStorage> GetExisitsHiveAsync()
	    {
		    return await Task.Run(() =>
		    {
			    lock (_locker)
			    {
				    return _hiveList.First();
			    }
		    });
	    }

		/// <summary> Метод добавления улья в пасеку
		/// </summary>
		/// <param name="beeEntity">Объект матки для которой необходимо создать улей</param>
		/// <returns></returns>
		public async Task<HiveStorage> CreateHiveAsync(QueenBeeEntity beeEntity)
	    {
		    return await Task.Run(() =>
		    {
			    lock (_locker)
			    {
				    var number = beeEntity.HiveNumber;
				    var newHive = new HiveStorage(number, 0, 0, 0, beeEntity, Model);
				    _hiveList.Add(newHive);
				    return newHive;
			    }
		    });
	    }

		/// <summary> Метод собирающий мед со всех пасек
		/// </summary>
		/// <returns>Задача сбора меда для асинхронной работы</returns>
	    public async Task AccumulateHoneyAsync()
		{
			await Task.Run(() =>
			{
				lock (_locker)
				{

					foreach (var hive in _hiveList)
					{
						AmountCollectedHoney += hive.CollectHoney();
						hive.HoneyCount = 0;
					}
				}
			});
		}

		/// <summary> Метод находит улей в пасеке
		/// </summary>
		/// <param name="hiveNumber">Номер улья</param>
		/// <returns>Задача поиска улья в пасеке для асинхронной работы</returns>
	    public async Task<HiveStorage> GetHiveAsync(int hiveNumber)
	    {
		    return await Task.Run(() =>
		    {
			    lock (_locker)
			    {
				    return _hiveList.FirstOrDefault(hive => hive.Number == hiveNumber);
			    }
		    });
	    }

		/// <summary> Метод удаления улья с пасеки
		/// </summary>
		/// <param name="hive">Удаляемое улей</param>
		/// <returns>Задача удаления улья из пасеки для асинхронной работы</returns>
	    public async Task DeleteHiveAsync(HiveStorage hive)
		{
			await Task.Run(() =>
			{
				lock (_locker)
				{
					_hiveList.Remove(hive);
				}
			});
		}
		#endregion

		#region Dispose
		public void Dispose()
		{
		}
		#endregion
	}
}
