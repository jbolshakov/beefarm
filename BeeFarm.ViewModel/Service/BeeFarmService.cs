using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Storage;
using Windows.UI.Xaml;
using BeeFarm.Model.Storage;
using BeeFarm.ViewModel.Common;

namespace BeeFarm.ViewModel.Service
{
	/// <summary> ����� ������������ ������� ��� �������� ������� ���������� �������
	/// </summary>
    public class BeeFarmService : IBeeFarmService
	{
		#region Private Properties
		//������� ���������� �����������: ������������ ���� SynchronizationContext �� ����������
		//private readonly SynchronizationContext _syncContext;
		//����������� DispatcherTimer ��� ��� �� �������� � ������ view, ���������� ���� ��� ���� �� �������
		private readonly DispatcherTimer _timer = new DispatcherTimer();

		//ThreadPoolTimer �������� � ����� ������, ��� �������� ���������� ���
		//private ThreadPoolTimer _timer2;

		//���������������� ���� ��������� �� ��������� ��������� ������
		private readonly BeeFarmConfig _config;
		//��������� ������ ������
        private Model.BeeModel _model;
		// �������� ���������� �������� ��������� ���� � ����� ������
        private TaskManager.TaskManager _taskManager;

		#endregion

		#region Public properties
		//��������� ������ ������
		public Model.BeeModel Model { get { return _model; } }
		//������� ���������� view
		public event Action Model_Changed;
		//������� ���������� ������
		public event Action Started;
		//������� ��������� ���������� ������
		public event Action Stoped;

		//���������� ���� ���������� ������
		public bool IsStarted
		{
			get
			{
				return _taskManager.IsStarted;
			}
		}
		#endregion

		public BeeFarmService(/*SynchronizationContext syncContext*/)
		{
			//_syncContext = syncContext;
			_timer.Interval = TimeSpan.FromMilliseconds(1000);
			_timer.Tick += timer_Tick;

			//���������� �������� ������=����������� ����
			_config = GetConfigAsync().Result;
		}

		#region Private methods
		//����� ����������� �� ������� ������� � �������� view � �����������
		private void timer_Tick(object sender, object e)
		{
			Model_Changed?.Invoke();
		}

		private async Task<BeeFarmConfig> GetConfigAsync()
	    {
			return await GetOrCreateConfigAsync();
		}

		/// <summary>
		/// ������� ���������������� ����
		/// </summary>
		/// <returns>���������������� ���� � ��������� ���������� ������</returns>
		private static async Task<BeeFarmConfig> GetOrCreateConfigAsync()
		{
			var confName = "BeeFarm.config";
			var currentDirectoryPath = Directory.GetCurrentDirectory();
			var configFilePath = string.Format("{0}\\{1}", currentDirectoryPath, confName);
			var currentDirectoryFolder = await StorageFolder.GetFolderFromPathAsync(currentDirectoryPath);

			BeeFarmConfig config = new BeeFarmConfig(new ApiaryConfig(new List<HiveConfig>() { new HiveConfig() }));

			if (!File.Exists(configFilePath))
			{
				await currentDirectoryFolder.CreateFileAsync(confName, CreationCollisionOption.ReplaceExisting);
				var fileTmp = await currentDirectoryFolder.GetFileAsync(confName);

				using (Stream stream = fileTmp.OpenStreamForWriteAsync().Result)
				{
					var serializer = new XmlSerializer(typeof(BeeFarmConfig));
					serializer.Serialize(stream, config);
				}
			}

			var file = await currentDirectoryFolder.GetFileAsync(confName);
			using (var stream = file.OpenStreamForReadAsync().Result)
			{
				var serializer = new XmlSerializer(typeof(BeeFarmConfig));

				config = (BeeFarmConfig)serializer.Deserialize(stream);
			}
			return config;
		}
		#endregion

		#region Public methods
		/// <summary> ����� �������������� ������, �������� ����� ������
		/// </summary>
		public void Initialize()
		{
			var hives = new Dictionary<int, int>();
			int i = 0;
			foreach (var hive in _config.Apiary.Hives)
			{
				hives.Add(++i, hive.BeeCount);
			}

			var model = new Model.BeeModel(hives);
			model.Initialize();
			_model = model;
			_taskManager = new TaskManager.TaskManager(_model);
		}

		/// <summary> ����� ��������� ������ ������ �� ����������
		/// </summary>
		/// <returns>������ ������� ���������� ������ ��� ������������ ����������</returns>
		public async Task RunAsync()
		{
			//���������� ������� ������� ���������� ������
			Started?.Invoke();

			_timer.Start();

			//_timer2 =
			//	ThreadPoolTimer.CreatePeriodicTimer(timer_Tick,
			//										TimeSpan.FromMilliseconds(1000));

			//���������� ��������� ��������� ����� ������
			await _taskManager.Start();
		}

		/// <summary> ����� ��������� ���� ������� ������
		/// </summary>
		public void Stop()
		{
			//���������� ������� ������������ ���������� ������
			Stoped?.Invoke();

			//������� ���� ������� ������
			_taskManager.Stop();

			//_timer2?.Cancel();
			_timer.Stop();
		}

		/// <summary> ����� ��������� ���������� ������ ����� ����
		/// </summary>
		/// <returns>������ ����� ���� ��� ������������ ����������</returns>
		public async Task AccumulateHoneyAsync()
		{
			await _model.ApiaryStorage.AccumulateHoneyAsync();
		}

		/// <summary> ����� ��������� ���������� ������ �������� ����
		/// </summary>
		/// <returns>������ �������� ���� ��� ������������ ����������</returns>
		public async Task DeleteHiveAsync(HiveStorage hive)
		{
			await _model.ApiaryStorage.DeleteHiveAsync(hive);
		}

		/// <summary> ����� ��������� ���������� ������ �������� ����
		/// </summary>
		/// <returns>������ �������� ���� ��� ������������ ����������</returns>
		public async Task CreateHiveAsync()
		{
			await _taskManager.CreateHiveAsync();
		}
		#endregion
	}
}
