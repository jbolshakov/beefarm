﻿using System;
using System.Threading.Tasks;
using BeeFarm.Model;
using BeeFarm.Model.Storage;

namespace BeeFarm.ViewModel.Service
{
	public interface IBeeFarmService
	{
		event Action Model_Changed;
		event Action Started;
		event Action Stoped;
		BeeModel Model { get; }
		bool IsStarted { get; }

		void Initialize();
		Task RunAsync();
		void Stop();
		Task AccumulateHoneyAsync();
		Task DeleteHiveAsync(HiveStorage hive);
		Task CreateHiveAsync();
	}
}
