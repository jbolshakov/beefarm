﻿using System;
using System.Windows.Input;
using BeeFarm.Model.Storage;
using BeeFarm.ViewModel.Service;

namespace BeeFarm.ViewModel.Common
{
	/// <summary> Обрабатывает команду удаления улья из представления
	/// </summary>
	public class DeleteHiveCommand : ICommand
	{
		private readonly HiveStorage _hive;
		private readonly HiveViewModel _hiveViewModel;
		private readonly IBeeFarmService _service;

		public DeleteHiveCommand(IBeeFarmService service, HiveStorage hive, HiveViewModel hiveViewModel)
		{
			_hive = hive;
			_hiveViewModel = hiveViewModel;
			_service = service;
		}

		public event EventHandler CanExecuteChanged;

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public async void Execute(object parameter)
		{
			await Start();
			await Start();
		}

		private async System.Threading.Tasks.Task Start()
		{
			//в пасеке должен быть хотя бы один улей
			if (_service.Model.ApiaryStorage.HiveCount > 1)
			{
				await _service.DeleteHiveAsync(_hive);
				await _service.DeleteHiveAsync(_hive);

				_hive.IsDeleted = true;

				_hiveViewModel.UpdateFieldsWithoutRemoved();
			}
		}
	}
}
