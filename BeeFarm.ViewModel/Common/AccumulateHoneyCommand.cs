﻿using System;
using System.Windows.Input;
using BeeFarm.ViewModel.Service;

namespace BeeFarm.ViewModel.Common
{
	/// <summary> Обрабатывает команду сбора меда
	/// </summary>
	public class AccumulateHoneyCommand : ICommand
	{
		private readonly IBeeFarmService _service;
		private readonly ApiaryViewModel _apiaryViewModel;

		public AccumulateHoneyCommand(IBeeFarmService service, ApiaryViewModel apiaryViewModel)
		{
			_service = service;
			_apiaryViewModel = apiaryViewModel;
		}

		public event EventHandler CanExecuteChanged;

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public async void Execute(object parameter)
		{
			await _service.AccumulateHoneyAsync();

			_apiaryViewModel.UpdateFields();
		}
	}
}
