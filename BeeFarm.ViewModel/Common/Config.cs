﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BeeFarm.ViewModel.Common
{
	[DataContract(Name = "BeeFarm", Namespace = "Config")]
	public class BeeFarmConfig
	{
		[DataMember]
		public ApiaryConfig Apiary { get; set; }

		public BeeFarmConfig()
		{
		}

		public BeeFarmConfig(ApiaryConfig apiary)
		{
			Apiary = apiary;
		}
	}

	[DataContract(Name = "Apiary", Namespace = "Config")]
	public class ApiaryConfig
	{
		[DataMember]
		public List<HiveConfig> Hives { get; set; }

		public ApiaryConfig()
		{
		}

		public ApiaryConfig(List<HiveConfig> hives)
		{
			Hives = hives;
		}
	}

	[DataContract(Name = "Hive", Namespace = "Config")]
	public class HiveConfig
	{
		[DataMember]
		public int BeeCount { get; set; }

		public HiveConfig()
		{
		}

		public HiveConfig(int beeCount)
		{
			BeeCount = beeCount;
		}
	}
}
