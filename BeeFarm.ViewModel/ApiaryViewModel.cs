﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using BeeFarm.Model.Storage;
using BeeFarm.ViewModel.Common;
using BeeFarm.ViewModel.Service;

namespace BeeFarm.ViewModel
{
    public class ApiaryViewModel : INotifyPropertyChanged
	{
		#region Private properties
		
		//вариант дальнейшей оптимизации: использовать один SynchronizationContext на приложение
		//вариант дальнейшей оптимзации при использовании обновления view в потоке xaml
		//private TaskScheduler _syncContextScheduler { get; set; }

		private readonly IBeeFarmService _service = ServiceLocator.GetService<IBeeFarmService>();

		private StartCommand _startCommand;

		private StopCommand _stopCommand;
		private AccumulateHoneyCommand _accumulateHoneyCommand;

		private CreateHiveCommand _createHiveCommand;

		private List<HiveStorage> _hives = new List<HiveStorage>();

		private bool _isStarted;

		private bool _isStoped;
		
		private int _totalBees;

		private int _hiveCount;

		private int _collectedHoney;

		#endregion

		#region Public properties
		/// <summary> Обеспечивает обновление интерфейса при изменениях в модели данных
		/// </summary>
		public ObservableCollection<HiveViewModel> Hives { get; private set; }

		public ICommand StartCommand { get { return _startCommand; } }

		public ICommand StopCommand { get { return _stopCommand; } }
		public ICommand AccumulateHoneyCommand { get { return _accumulateHoneyCommand; } }

		public ICommand CreateHiveCommand { get { return _createHiveCommand; } }

		/// <summary> Флаг активности пасеки
		/// </summary>
		public bool IsStarted
		{
			get { return _isStarted; }
			set
			{
				if (_isStarted != value)
				{
					_isStarted = value;
					OnPropertyChanged("IsStarted");
				}
			}
		}

		/// <summary> Флаг остановки активности улья
		/// </summary>
		public bool IsStoped
		{
			get { return _isStoped; }
			set
			{
				if (_isStoped != value)
				{
					_isStoped = value;
					OnPropertyChanged("IsStoped");
				}
			}
		}
		public int TotalBees
		{
			get { return _totalBees; }
			set
			{
				if (_totalBees != value)
				{
					_totalBees = value;
					OnPropertyChanged("TotalBees");
				}
			}
		}

		public int HiveCount
		{
			get { return _hiveCount; }
			set
			{
				if (_hiveCount != value)
				{
					_hiveCount = value;
					OnPropertyChanged("HiveCount");
				}
			}
		}

		public int CollectedHoney
		{
			get { return _collectedHoney; }
			set
			{
				if (_collectedHoney != value)
				{
					_collectedHoney = value;
					OnPropertyChanged("CollectedHoney");
				}
			}
		}

		#endregion

		public ApiaryViewModel()
		{
			Hives = new ObservableCollection<HiveViewModel>();

			SetCommands();

			//if (SynchronizationContext.Current != null)
			//{
			//	_syncContextScheduler = TaskScheduler.FromCurrentSynchronizationContext();
			//}
			//else
			//{
			//	_syncContextScheduler = TaskScheduler.Current;
			//}

			UpdateFields();

			IsStarted = _service.IsStarted;
			IsStoped = _service.IsStarted == false;

			//подписываемся на обновление модели
			_service.Model_Changed += Model_Changed;

			_service.Started += ServiceStarted;
            _service.Stoped += ServiceStoped;
		}

		#region Changed
		public event PropertyChangedEventHandler PropertyChanged;
		private async void OnPropertyChanged(string propertyName)
		{
			//вариант дальнейшей оптимзации при использовании обновления view в потоке xaml
			//await Task.Factory.StartNew(() =>
			//{
			//	PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			//}, new System.Threading.CancellationToken(), TaskCreationOptions.PreferFairness, _syncContextScheduler);

			//await Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
			// PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName)));

			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		/// <summary> Подписка на событие обновления объектной модели
		/// </summary>
		private void Model_Changed()
		{
			UpdateFields();
		}

		#endregion

		#region Public methods
		public void UpdateFields()
		{
			_hives = _service.Model.ApiaryStorage.HiveList.ToList();

			//вариант дальнейшей оптимзации при использовании обновления view в потоке xaml
			//Task.Factory.StartNew(SetHivesView, new System.Threading.CancellationToken(), TaskCreationOptions.PreferFairness, _syncContextScheduler).ConfigureAwait(false); ;

			SetHivesView();

			TotalBees = _service.Model.ApiaryStorage.BeeCount;
			HiveCount = _service.Model.ApiaryStorage.HiveCount;
			CollectedHoney = _service.Model.ApiaryStorage.AmountCollectedHoney;
		}
		#endregion

		#region Private methods
		//обновление view улья
		private void SetHivesView()
		{
			foreach (var hive in _hives.Where(row => !row.IsDeleted).OrderBy(hive => hive.Number))
			{
				var hiveView = new HiveViewModel(_service, hive);
				if (Hives.Contains(hiveView))
				{
					Hives.Remove(hiveView);
					Hives.Add(hiveView);
				}
				else
				{
					Hives.Add(hiveView);
				}

				hiveView.IsStarted = IsStarted;
				hiveView.UpdateFields();
			}
		}

		private void SetCommands()
		{
			_startCommand = new StartCommand(_service, this);
			_stopCommand = new StopCommand(_service, this);
			_accumulateHoneyCommand = new AccumulateHoneyCommand(_service, this);
			_createHiveCommand = new CreateHiveCommand(_service, this);
		}

		private void ServiceStarted()
        {
			IsStarted = true;
			IsStoped = false;			
        }

        private void ServiceStoped()
        {
            IsStarted = false;
			IsStoped = true;	
        }
		#endregion
	}
}
