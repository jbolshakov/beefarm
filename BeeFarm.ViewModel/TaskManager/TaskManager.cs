﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BeeFarm.Model;
using BeeFarm.Model.Entity;

namespace BeeFarm.ViewModel.TaskManager
{
	/// <summary> Менеджер управления задачами улья
	/// </summary>
    public class TaskManager
    {
		#region Properties
		//объектное представление пасеки
		private volatile BeeModel _model;
		//свойство остановки работы задач
		private volatile CancellationTokenSource _tokenSource;
		//массив типов пчел
		private readonly Array _values = Enum.GetValues(typeof(BeeTypeEnum));
		private readonly Random _random = new Random();
		//Флаг работы задач улья
		public bool IsStarted { get { return _tokenSource?.IsCancellationRequested == false; } }
		#endregion

		/// <summary> Конструктор менеджера задач пасеки
		/// </summary>
		/// <param name="model">Объектная модель пасеки</param>
		public TaskManager(BeeModel model)
        {
			_model = model;
        }

		#region Public method
		/// <summary> Метод запуска генерации задач улья
		/// </summary>
		public System.Threading.Tasks.Task Start()
        {
			_tokenSource = new CancellationTokenSource();

			var result = System.Threading.Tasks.Task.Run(() =>
            {
	            while(true)
	            {
		            List<System.Threading.Tasks.Task> tasks;

					RunApriaryGeneration(out tasks);

		            if (_tokenSource.Token.IsCancellationRequested)
			            return;

		            System.Threading.Tasks.Task.WaitAll(tasks.ToArray());
	            }
            });

	        return result;
        }

		/// <summary> Метод генерации улья
		/// </summary>
		/// <param name="tasks">Задачи по генерации улья</param>
	    private void RunApriaryGeneration(out List<System.Threading.Tasks.Task> tasks)
	    {
			//список воспроизводящих собирающих пчел маток
		    var queenBees = new List<BeeEntity>();
			//список собирающих мед пчел
		    var collectBees = new List<BeeEntity>();
			//список пчел охранников 
			var securityBees = new List<BeeEntity>();

			//считываем состояние пасеки после предыдущей генерации
			using (var apiary = _model.ApiaryStorage)
		    {
			    for (var i = 0; i < apiary.HiveList.Count(); i++)
			    {
				    queenBees.Add(apiary.HiveList[i].QueenBeeEntity);
					collectBees.AddRange(apiary.HiveList[i].CollectBees);
					securityBees.AddRange(apiary.HiveList[i].SecurityBees);

				}
		    }

		    tasks = new List<System.Threading.Tasks.Task>();

			//проверяем не застали ли нас в процессе отмены внутри метода
		    if (_tokenSource.Token.IsCancellationRequested)
			    return;

		    var type = (BeeTypeEnum)_values.GetValue(_random.Next(_values.Length));

			//генерируем королеву, королева генерирует собирающих мед пчел
		    if (type == BeeTypeEnum.Queen || !collectBees.Any())
		    {
				//проверяем не застали ли нас в процессе отмены внутри метода
				if (_tokenSource.Token.IsCancellationRequested)
				    return;

				CreateQueenTask(queenBees, _random.Next(0, queenBees.Count), tasks, collectBees);
		    }

			//создаем охранника
			if (type == BeeTypeEnum.Security)
			{
				//проверяем не застали ли нас в процессе отмены внутри метода
				if (_tokenSource.Token.IsCancellationRequested)
					return;

				CreateSecurityTask(queenBees, _random.Next(0, queenBees.Count), tasks, securityBees);
			}

			//собираем мед, проверяем, что не собираем мед с не созданного улья
			if (type == BeeTypeEnum.Collect && collectBees.Count > 1)
		    {
				//проверяем не застали ли нас в процессе отмены внутри метода
				if (_tokenSource.Token.IsCancellationRequested)
				    return;

			    AccumulateHoney(collectBees, securityBees, _random.Next(0, collectBees.Count), _random.Next(0, securityBees.Count), tasks);
		    }
	    }

		/// <summary> Оставнавливаем все задачи
		/// </summary>
	    public void Stop()
		{
			_tokenSource.Cancel();
		}

		/// <summary> Создаем новый улей
		/// </summary>
		/// <returns>Задача создания улья асинхронно</returns>
		public async System.Threading.Tasks.Task CreateHiveAsync()
		{
			var newBee = new QueenBeeEntity(
											_model.ApiaryStorage.QueenCount, 
											_model.ApiaryStorage.HiveCount + 1);

			await _model.ApiaryStorage.CreateHiveAsync(newBee);
		}
		#endregion

		#region Private method
		/// <summary> Задача генерации собирающих мед пчел
		/// </summary>
		/// <param name="queenBees">Список доступных маток</param>
		/// <param name="randomBee">Случайный номер пчелы</param>
		/// <param name="tasks">Список задач генерирующих улей</param>
		/// <param name="securityBee">Список всех пчел пасеки, в том числе удаленных</param>
		private void CreateSecurityTask(List<BeeEntity> queenBees, int randomBee, List<System.Threading.Tasks.Task> tasks, List<BeeEntity> securityBee)
		{
			var queenBee = (QueenBeeEntity)queenBees[randomBee];

			using (var apiary = _model.ApiaryStorage)
			{
				var hive = apiary.GetHiveAsync(queenBee.HiveNumber).Result;

				if (hive != null)
				{
					tasks.Add(System.Threading.Tasks.Task.Run(() =>
					{
						var newBee = new SecurityBeeEntity(hive.GetNumberForNewBee() + 1, hive.Number);
						hive.CreatSecurityBee(newBee);
					}));
				}
				else
				{
					//удаляем пчел несуществующего улья
					securityBee.RemoveAll(row => row.HiveNumber == queenBee.HiveNumber);
				}
			}
		}


		/// <summary> Задача генерации собирающих мед пчел
		/// </summary>
		/// <param name="queenBees">Список доступных маток</param>
		/// <param name="randomBee">Случайный номер пчелы</param>
		/// <param name="tasks">Список задач генерирующих улей</param>
		/// <param name="collectBee">Список всех пчел пасеки, в том числе удаленных</param>
		private void CreateQueenTask(List<BeeEntity> queenBees, int randomBee, List<System.Threading.Tasks.Task> tasks, List<BeeEntity> collectBee)
	    {
		    var queenBee = (QueenBeeEntity) queenBees[randomBee];

		    using (var apiary = _model.ApiaryStorage)
		    {
			    var hive = apiary.GetHiveAsync(queenBee.HiveNumber).Result;

			    if (hive != null)
			    {
				    tasks.Add(System.Threading.Tasks.Task.Run(() =>
											{
												var newBee = new CollectBeeEntity(hive.GetNumberForNewBee() + 1, hive.Number);
												hive.CreateCollectBee(newBee);
											}));
			    }
			    else
			    {
				    //удаляем пчел несуществующего улья
				    collectBee.RemoveAll(row => row.HiveNumber == queenBee.HiveNumber);
			    }
		    }
	    }

		/// <summary> Задача генерации меда
		/// </summary>
		/// <param name="collectBees"></param>
		/// <param name="securityBees"></param>
		/// <param name="randomSecurityBee"></param>
		/// <param name="randomBee">Случайный номер пчелы в улье</param>
		/// <param name="tasks">задача сбора меда для асинхронного выполнения</param>
		private void AccumulateHoney(List<BeeEntity> collectBees, List<BeeEntity> securityBees, int randomBee, int randomSecurityBee, List<Task> tasks)
	    {
			//могли случайно зайти до начала генерации первой пчелы в улье
		    if (collectBees.Any() && securityBees.Any())
		    {
			    var beeSecurity = (SecurityBeeEntity) securityBees[randomSecurityBee];
				var bee = (CollectBeeEntity)collectBees[randomBee];

				//проверяем соостветсвие охранника и собирающей пчелы
			    if (bee.HiveNumber > beeSecurity.HiveNumber)
			    {
				    using (var apiary = _model.ApiaryStorage)
				    {
					    var hive = apiary.GetHiveAsync(bee.HiveNumber).Result;
					    //улей может быть удален
					    if (hive != null)
					    {
						    tasks.Add(System.Threading.Tasks.Task.Run(() => { hive.AccumulateHoney(); }));
					    }
					    else
					    {
						    //удаляем пчел несуществующего улья
						    collectBees.RemoveAll(row => row.HiveNumber == bee.HiveNumber);
					    }
				    }
			    }
		    }
	    }

	    #endregion
	}
}