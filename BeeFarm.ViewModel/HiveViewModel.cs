﻿using System.ComponentModel;
using System.Linq;
using BeeFarm.Model.Storage;
using BeeFarm.ViewModel.Common;
using BeeFarm.ViewModel.Service;

namespace BeeFarm.ViewModel
{
	/// <summary> Модель представления улья
	/// </summary>
    public class HiveViewModel : INotifyPropertyChanged
	{ 
		#region Properties
		//вариант дальнейшей оптимизации: использовать один SynchronizationContext на приложение
		//вариант дальнейшей оптимзации при использовании обновления view в потоке xaml
		//public TaskScheduler syncContextScheduler { get; set; }

		private readonly IBeeFarmService _service;
        private readonly HiveStorage _hive;

		private bool _isStarted;
		public bool IsStarted {
			get { return _isStarted; }
			set
			{
				_isStarted = value;
				OnPropertyChanged("IsStarted");
			}
		}

		public int Number
		{
			get { return _hive.Number; }
		}
		
		private volatile int _honeyCount;
		public int HoneyCount
		{
			get { return _honeyCount; }
			set
			{
				if (_honeyCount != value)
				{
					_honeyCount = value;
					OnPropertyChanged("HoneyCount");
				}
			}
		}

		private volatile int _totalBees;
		public int TotalBees
		{
			get { return _totalBees; }
			set
			{
				//if (_totalBees != value)
				{
					_totalBees = value;
					OnPropertyChanged("TotalBees");
				}
			}
		}

		private volatile int _collectBees;
		public int CollectBees
		{
			get { return _collectBees; }
			set
			{
				if (_collectBees != value || _collectBees == 0 | _collectBees == 0)
				{
					_collectBees = value;
					OnPropertyChanged("CollectBees");
				}
			}
		}

		private volatile int _securityCount;
		public int SecurityCount
		{
			get { return _securityCount; }
			set
			{
				//if (_securityCount != value)
				{
					_securityCount = value;
					OnPropertyChanged("SecurityCount");
				}
			}
		}

		//королева в улье всегда одна
		private volatile int _queenBeeCount = 1;
		public int QueenBeeCount
		{
			get { return _queenBeeCount; }
			set
			{
				if (_queenBeeCount != value)
				{
					_queenBeeCount = value;
					OnPropertyChanged("QueenBeeCount");
				}	
			}
		}

		#endregion

		public HiveViewModel(IBeeFarmService service, HiveStorage hive)
        {
			_hive = hive;
			_service = service;
			_deleteHiveCommand = new DeleteHiveCommand(_service, _hive, this);

			//вариант дальнейшей оптимзации при использовании обновления view в потоке xaml
			//вариант дальнейшей оптимизации: использовать один SynchronizationContext на приложение
			//if (SynchronizationContext.Current != null)
			//{
			//	syncContextScheduler = TaskScheduler.FromCurrentSynchronizationContext();
			//}
			//else
			//{
			//	syncContextScheduler = TaskScheduler.Current;
			//}

			UpdateFields();

			_service.Model_Changed += Model_Changed;
			_service.Started += ServiceStarted;
			_service.Stoped += ServiceStoped;
		}

		#region PropertyChanged
		public event PropertyChangedEventHandler PropertyChanged;

		private /*async Task*/ void OnPropertyChanged(string propertyName)
		{
			//вариант дальнейшей оптимзации при использовании обновления view в потоке xaml
			//await Task.Factory.StartNew(() =>
			//{
			//	PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			//}, new System.Threading.CancellationToken(), TaskCreationOptions.PreferFairness, syncContextScheduler).ConfigureAwait(false); ;

			//await Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
			// PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName)));

			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion

		#region Commands
		private readonly DeleteHiveCommand _deleteHiveCommand;
		public DeleteHiveCommand DeleteHiveCommand
	    {
			get { return _deleteHiveCommand; }
		}
		#endregion

		#region Public methods
		public void UpdateFields()
		{
			//если королева удалена, то и улей удален, обновлять остальные поля нет смысла
			if (QueenBeeCount > 0)
			{
				SecurityCount = _hive.SecurityBees.Count();
				HoneyCount = _hive.HoneyCount;
				TotalBees = _hive.TotalBee;
				CollectBees = _hive.CollectBees.Count();
			}

			IsStarted = true;
		}

		/// <summary> Обновление полей удаленных сущностей, например, для изменения текста 
		/// </summary>
		public void UpdateFieldsWithoutRemoved()
	    {
			HoneyCount = _hive.HoneyCount = 0;
			TotalBees = _totalBees = 0;
			CollectBees = _collectBees = 0;
			QueenBeeCount = 0;
			SecurityCount = _securityCount = 0;

			//выключаем активность пасеки
			IsStarted = false;
	    }

		#endregion

		#region Private methods
		private void Model_Changed()
		{
			if (IsStarted)
				UpdateFields();
		}

		private void ServiceStarted()
		{
			IsStarted = true;
		}

		private void ServiceStoped()
		{
			IsStarted = false;
		}

		#endregion

		#region Equals
		public override bool Equals(object obj)
		{
			if (obj == null || typeof(HiveViewModel) != obj.GetType())
				return false;

			if (ReferenceEquals(this, obj))
				return true;

			return Equals((HiveViewModel)obj);
		}

		public bool Equals(HiveViewModel other)
		{
			if (other == null)
				return false;

			if (ReferenceEquals(this, other))
				return true;

			return Number == other.Number;
		}

		public override int GetHashCode()
		{
			return Number;
		}

		#endregion
	}
}
